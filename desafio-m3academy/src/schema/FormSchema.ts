import * as Yup from "yup"
import { phoneNumber } from "../utils/validações";
import { cpfNumber } from "../utils/validações";
import { dataNascimento } from "../utils/validações";

export default Yup.object().shape({
    
    email: Yup.string().required("Campo Obrigatório!").email('E-mail inválido!'),
    name: Yup.string().min(3,"Nome Inválido!").required("Campo Obrigatório!"),
    insta: Yup.string().required("Campo Obrigatório!"),
    tel: Yup.string().matches(phoneNumber,"Telefone inválido!").required("Campo Obrigatório!"),
    data: Yup.string().matches(dataNascimento, "Data inválida!").required("Campo Obrigatório!"),
    cpf: Yup.string().matches(cpfNumber,"Cpf Inválido!").required("Campo Obrigatório!"),
});


