import React from "react";
import { string } from "yup";

export const phoneNumber = /\([1-9]{2}\) 9[0-9]\d{3}-\d{4}/
export const cpfNumber = /[0-9]{3}.[0-9]\d{2}.\d{3}-\d{2}/
export const dataNascimento = /[1-2][0-9].[0-1][0-9].[1-2][0-9]{3}/
