import React from "react";
import {Formik, Field, Form, ErrorMessage} from 'formik';
import FormSchema from '../../schema/FormSchema'
import './newsletter.scss'


interface IFormikValues {
    email:string;
}

const initialValues = {
    email:"",
}



export function Newsletter(){

    const handleFormikSubmit = (values: IFormikValues) => {
        console.log(values);
        
        values.email = "";
    };

    return  (
        <>
            <Formik onSubmit={handleFormikSubmit} initialValues={initialValues} validationSchema={FormSchema}>
                
                {({errors,touched})=>(

                    <div className="footer-newsletter">

                        <div className="footer-newsletter__content">

                            <h3 className="footer-newsletter__title">ASSINE NOSSA NEWSLETTER</h3>
                            
                            <Form className="footer-form">

                                <div className="footer-form__content-input">
                                    <Field id="email" className={errors.email && touched.email && 'invalid'}  name="email" placeholder="E-mail" />
                                    <ErrorMessage component="span" name="email" className="footer-form__error" />
                                </div>
                                <button className="footer-form__button" type="reset">ENVIAR</button>
                            </Form>
                        </div>
                    </div>
                )}

            </Formik>
        </>
    );
}