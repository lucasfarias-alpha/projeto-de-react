import React, { useState} from "react";
import './footer.scss';
import { Newsletter } from '../newsletter/newsletter';

import whatsappIcon from '../../imagens/whatsapp-icon.png';
import setaFooter from '../../imagens/seta-footer.png';
import faceIcon from '../../imagens/facebook-icon.png';
import instaIcon from '../../imagens/instagram-icon.png';
import twitterIcon from '../../imagens/twitter-icon.png';
import youtubeIcon from '../../imagens/youtube-icon.png';
import linkedinIcon from '../../imagens/linkedin-icon.png';
import masterCard from '../../imagens/master-card.png';
import visaCard from '../../imagens/visa-card.png';
import dinersCard from '../../imagens/Diners-card.png';
import eloCard from '../../imagens/Elo.png';
import hiperCard from '../../imagens/Hiper.png';
import payPal from '../../imagens/Pagseguro.png';
import Boleto from '../../imagens/Boleto.png';
import Certificado from '../../imagens/certificado.png';
import iconCompletoVtex from '../../imagens/icon-completo-vtex.png';
import Barra from '../../imagens/barra-footer.png';
import Cruz from '../../imagens/cruz-mobile.png';



 
export function Footer(){
    
    const [options, setOptions] = useState(false);
    const [open, SetOpen] = useState(false);
    const [abrir, setAbrir] = useState(false);
    const toggle=()=>{
        setOptions(!options);
    }
    const Open=()=>{
        SetOpen(!open);
    }
    const Abrir=()=>{
        setAbrir(!abrir);
    }
    const [ pageYPosition, setPageYPosition ] = useState(0);

    function getPageYAfterScroll(){
        setPageYPosition(window.scrollY);
    }

    window.addEventListener('scroll', getPageYAfterScroll);



    return (
        <> 
            <section className="footer">
                <div className="newsletter">
                    
                    <Newsletter />

                    <div className="newsletter-image-direita">

                        <img className="newsletter-image-direita__image-whatsapp" src={whatsappIcon} alt="icone do whatsapp" />

                        <a href="#" className="newsletter-image-direita__div-seta" >
                            <img className="newsletter-image-direita__setinha" src={setaFooter} alt="icone da seta para o topo" />
                        </a>
                    </div>
                </div>

                <div className="mid-footer">

                    <nav className="mid-footer__wrapper">

                        <div onClick={toggle} className="mid-footer__options">

                            <div className="mid-footer__principal">

                                <h3 className="mid-footer__title">
                                    INSTITUCIONAL 
                                </h3>
                                    
                                <img className="cruz-mobile" src={Cruz} alt="" />
        
                            </div>
                            <ul className={"links " + (options ? "aberto" : "")}>

                                <li className="option-link"> <a href="#">Quem Somos</a></li>
                                <li className="option-link"> <a href="#">Política de Privacidade</a></li>
                                <li className="option-link"> <a href="#">Segurança</a></li>
                                <li className="option-link"> <a href="#">Seja um Revendedor</a></li>

                            </ul>

                        </div>

                        <div onClick={Open} className="mid-footer__options">

                            <div className="mid-footer__principal">

                                <h3 className="mid-footer__title">DÚVIDAS</h3>

                                <img className="cruz-mobile" src={Cruz} alt="" />

                            </div>
                            <ul className={"links2 " + (open ? "open" : "")}>

                                <li className="option-link"> <a href="#">Entrega</a></li>
                                <li className="option-link"> <a href="#">Pagamento</a></li>
                                <li className="option-link"> <a href="#">Trocas e Devolução</a></li>
                                <li className="option-link"> <a href="#">Dúvidas Frequentes</a></li>
                            </ul>

                        </div>

                        <div onClick={Abrir} className="mid-footer__options">

                            <div className="mid-footer__principal">

                                <h3 className="mid-footer__title">FALA CONOSCO</h3>

                                <img className="cruz-mobile" src={Cruz} alt="" />

                            </div>
                            <ul className={"links3 " + (abrir ? "abrir" : "")}>

                                <li className="option-link"> <a href="#">Atendimento ao Consumidor</a></li>
                                <li className="option-link"> <a href="#">(11)4159-9504</a></li>
                                <li className="option-link"> <a href="#">Atendimento Online</a></li>
                                <li className="option-link"> <a href="#">(11)99433-8825</a></li>
                        
                            </ul>
                        </div>

                        <div className="mid-footer-icons">

                            <img className="mid-footer-icons__icon" src={faceIcon} alt="icone do facebook" />
                            <img className="mid-footer-icons__icon" src={instaIcon} alt="icone do instagram" />
                            <img className="mid-footer-icons__icon" src={twitterIcon} alt="icone do twitter" />
                            <img className="mid-footer-icons__icon" src={youtubeIcon} alt="icone do youtube" />
                            <img className="mid-footer-icons__icon" src={linkedinIcon} alt="icone do linkedin" />
                            <p>www.loremipsum.com</p>
                        </div>
                    </nav>


                </div>
                <div className="footer-inferior">

                    <div className="icons-cards icons-cards-mobile">
                        <img className="icons-cards__icon" src={masterCard} alt="icone masterCard" />
                        <img className="icons-cards__icon" src={visaCard} alt="icone Visa" />
                        <img className="icons-cards__icon" src={dinersCard} alt="Icone american express" />
                        <img className="icons-cards__icon" src={eloCard} alt="icone eloCard" />
                        <img className="icons-cards__icon" src={hiperCard} alt="icone HiperCard" />
                        <img className="icons-cards__icon" src={payPal} alt="icone paypal" />
                        <img className="icons-cards__icon" src={Boleto} alt="icone boleto" />
                        <img className="icons-cards__barra" src={Barra} alt="barra de divisão" />
                        <img className="icon-card" src={Certificado} alt="certificado da vtex" />
                    </div>

                    <div className="footer-inferior__text">

                        <p className="footer-inferior__description">Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit, sed do eiusmod tempor
                        </p>
                    </div>
                    <div className="icons-cards icons-cards-desktop">
                        <img className="icons-cards__icon" src={masterCard} alt="icone masterCard" />
                        <img className="icons-cards__icon" src={visaCard} alt="icone Visa" />
                        <img className="icons-cards__icon" src={dinersCard} alt="Icone american express" />
                        <img className="icons-cards__icon" src={eloCard} alt="icone eloCard" />
                        <img className="icons-cards__icon" src={hiperCard} alt="icone HiperCard" />
                        <img className="icons-cards__icon" src={payPal} alt="icone paypal" />
                        <img className="icons-cards__icon" src={Boleto} alt="icone boleto" />
                        <img className="icons-cards__barra" src={Barra} alt="barra de divisão" />
                        <img className="icon-card__icon-maior" src={Certificado} alt="certificado da vtex" />
                    </div>
                    <div>
                        <img src={iconCompletoVtex} alt="icones vtex do rodapé" />
                    </div>
                </div>
            </section>

        </>
    )
}