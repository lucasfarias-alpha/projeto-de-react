import React from 'react';
import './main.scss';
import { useState } from 'react';
import {CustomForm} from '../formulario/CustomForm'


import casaTopMain from '../../imagens/casa-top-main.png';
import setaTopMain from '../../imagens/seta-top-main.png';

export function Main(){

    const [abrirSobre, setAbrirSobre] = useState(true);
    const [abrirContato, setAbrirContato] = useState(false);

    const toggleAbrirSobre =()=>{

        setAbrirSobre(!abrirSobre);
        if(abrirContato){
            setAbrirContato(false);
        }
    }

    const handleOpenContato =()=>{
        setAbrirContato(!abrirContato);
        if(abrirSobre){
            setAbrirSobre(false);
        }
    }
    

    return (
        <>
            <div className='container'>
                
                <div className='main-top'>

                    
                    <img src={  casaTopMain } alt="casinha do topo" />
                    <img className='main-top__seta' src={  setaTopMain } alt="setinha do topo" />
                    <p className='main-top__description'>institucional</p>

                </div>

                
                <div className='main-title'>

                    <h2 className='main-title__title'>
                        institucional
                    </h2>
                </div>
                <div className='main-global'>

                    <ul className='main-buttons'>

                        <li onClick={toggleAbrirSobre} className='main-buttons__button'>

                            <a className={"main-buttons__link " + (abrirSobre ? "color" : "")} href="#">Sobre</a> 
                        </li>

                        <li className='main-buttons__button'>

                            <a className="main-buttons__link" href="#">Forma de Pagamento</a>
                        </li>

                        <li className='main-buttons__button'>

                            <a className="main-buttons__link" href="#">Entrega</a> 
                        </li>
                        <li className='main-buttons__button'>

                            <a className="main-buttons__link"  href="#">Troca e Devolução</a> 
                        </li>

                        <li className='main-buttons__button'>

                            <a className="main-buttons__link" href="#">Segurança e Privacidade</a>
                        </li>

                        <li onClick={handleOpenContato} className='main-buttons__button'>

                            <a className={"main-buttons__link " + (abrirContato ? "color" : "")} href="#">Contato</a> 

                        </li>
                    </ul>

                    <div className={"formulario-contents " + (abrirContato ? "abrir-form" : "")}>

                        <CustomForm />
                    </div>
                    

                    <div className={"main-texts " + (abrirSobre ? "abrir" : "")}>

                        <h2 className='main-texts__title'>
                            Sobre
                        </h2>

                        <p className='main-texts__description'>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                            Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                            Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                        </p>
                    </div>
                </div>

                
            </div>

        </>
    );
};