import React, { useState } from "react";
import './CustomForm.scss';
import FormSchema from '../../schema/FormSchema';

import { Formik, Form, Field, ErrorMessage } from "formik";

interface IFormikValues {
    name:String;
    email:String;
    cpf:String;
    data:String;
    tel:String;
    insta:String;
    
};


const initialValues = {
    name:"",
    email:"",
    cpf:"",
    data:"",
    tel:"",
    insta:"",
};

export function CustomForm(){
    
    const handleFormikSubmit = (values: IFormikValues) => {
        console.log(values);
        

        values.name = "";
        values.email = "";
        values.cpf = "";
        values.data = "";
        values.tel = "";
        values.insta = "";
        
    };
    

    return(
        <>
                <Formik 
                    onSubmit={handleFormikSubmit}
                    initialValues={initialValues}
                    validationSchema={FormSchema}>
                    
                    {({errors, touched}) => (
                        
                        
                        <Form className="formulario">

                                <h3 className="formulario__title">
                                    PREENCHA O FORMULÁRIO
                                </h3>
                                <div className="formulario__div-inputs">

                                    <label className="formulario__label" htmlFor="name">Name</label>
                                    <Field  className={errors.name && touched.name && "invalid"} id="name" name="name" placeholder="Seu nome completo"/>
                                    <ErrorMessage className="formulario__error" component="span" name="name" />
                                </div>

                                <div className="formulario__div-inputs">

                                    <label className="formulario__label" htmlFor="email">E-mail</label>
                                    <Field className={errors.email && touched.email && "invalid"} id="email" name="email" placeholder="Seu e-mail" />
                                    <ErrorMessage className="formulario__error" component="span" name="email" />
                                </div>

                                <div className="formulario__div-inputs">

                                    <label className="formulario__label" htmlFor="cpf">CPF:</label>
                                    <Field className={errors.cpf && touched.cpf && "invalid"} id="cpf" name="cpf" placeholder="000.000.000-00" />
                                    <ErrorMessage className="formulario__error" component="span" name="cpf" />
                                </div>

                                <div className="formulario__div-inputs">

                                    <label className="formulario__label" htmlFor="data">Data de Nascimento:</label>
                                    <Field className={errors.data && touched.data && "invalid"} id="data" name="data" placeholder="00.00.0000"/>
                                    <ErrorMessage className="formulario__error" component="span" name="data" />
                                </div>

                                <div className="formulario__div-inputs">

                                    <label className="formulario__label" htmlFor="tel">Telefone:</label>
                                    <Field className={errors.tel && touched.tel && "invalid"} id="tel" name="tel" placeholder="(00) 00000-0000"/>
                                    <ErrorMessage className="formulario__error" component="span" name="tel" />
                                </div>

                                <div className="formulario__div-inputs">

                                    <label className="formulario__label" htmlFor="insta">Instagram</label>
                                    <Field className={errors.insta && touched.insta && "invalid"} id="insta" name="insta" placeholder="@seuuser"/>
                                    <ErrorMessage className="formulario__error" component="span" name="insta" />
                                </div>
                                <div className="formulario__div-declaration">
                                    <label className="formulario__label-declaration" htmlFor="declaration">*Declaro que li e aceito!</label>
                                    <input type="checkbox" name="declaration" id="declaration" />
                                </div>

                                <button type="submit" className="formulario__button">CADASTRE-SE</button>
                        </Form>
                        

                    )}

                </Formik>

        </>
    )
    
}