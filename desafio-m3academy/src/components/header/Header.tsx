import imgLogo from '../../imagens/logo-m3academy.png';
import imgLupa from '../../imagens/lupa-busca.png';
import imgCarrinho from '../../imagens/shopping-cart.png';
import imgCarrinhoMobile from '../../imagens/shopping-cart-mobile.png';
import menuIcon from '../../imagens/barras-menu.png';
import setaMobile from '../../imagens/seta-mobile.png';
import iconMenuMobile from '../../imagens/icon-menu-mobile.png';
import cadeadoMobile from '../../imagens/cadeado-mobile.png';
import fechaMenuMobile from '../../imagens/fecha-menu-mobile.png';
import voltarMenuMobile from '../../imagens/voltar-menu-mobile.png';

import './Header.scss';
import React from 'react';
import { useState } from 'react'; 


export function Header() {

    const [ menuMobile, setMenuMobile ] = useState(false);
    const [ subMenuMobile, setSubMenuMobile ] = useState(false);
    const [ subMenuMobile2, setSubMenuMobile2 ] = useState(false);
    
    function handleOpenMenuMobile(){
        setMenuMobile(true)
    }
    function handleCloseMenuMobile() {
        setMenuMobile(false)
    }
    function handleOpenSubMenuMobile(e:any){
        e.preventDefault()
        setSubMenuMobile(true)
    }
    function handleCloseSubMenuMobile() {
        setSubMenuMobile(false)
    }
    function handleOpenSubMenuMobile2(e:any){
        e.preventDefault()
        setSubMenuMobile2(true)
    }
    function handleCloseSubMenuMobile2() {
        setSubMenuMobile2(false)
    }

    
    return (
        <>
            <header className="page-header">

                <div className="container">

                    <div className="page-header-superior">

                        <div className='page-header-superior__menu-mobile'>
                            <button onClick={handleOpenMenuMobile} className='menu-button-mobile'>
                                <img className='menu-icon-mobile' src={menuIcon} alt="menu" />
                            </button>
                        </div>

                        <div className='page-header-superior__logo'>
                            <img className='header-logo' src={ imgLogo } alt="Logo m3-academy" />
                        </div>

                        <div className='page-header-superior__div-input'>
                            
                            <input className='input' placeholder='busca...' type="text" />
                            <img className='lupa-busca' src={imgLupa} alt="Lupa da busca" />
                            
                        </div>

                        <div className='page-header-superior__shopping-cart'>

                            <button className='shopping-cart-button'>Entra</button>

                            <a className='images-carts' href="/">
                                <img className='image-cart' src={imgCarrinho} alt="Carrinho de compras" />
                                <img className='image-cart-mobile' src={imgCarrinhoMobile} alt="carrinho-mobile" />
                            </a>
                        </div>

                    </div>
                    <div className='header-div-input-mobile'>
                                
                        <input className='header-div-input-mobile__input' placeholder='busca...' type="text" />
                        <img className='header-div-input-mobile__lupa' src={imgLupa} alt="Lupa da busca" />
                    </div>
                    
                </div>
                <div className="page-header-inferior">


                    <nav className={`${'menu-header'} ${menuMobile && 'active'}`}>

                        <div className='menu-mobile-top'>
                            <div className='menu-mobile-top__wrapper'>
                                <div className='menu-mobile-text'>
                                    <img className='menu-mobile-image' src= { cadeadoMobile } alt="" />
                                    COMPRA 100% SEGURA
                                </div>

                                <button onClick={handleCloseMenuMobile} className='menu-mobile-close-button'>
                                    <img src={fechaMenuMobile} alt="" />
                                </button>
                            </div>
                        </div>
                        
                        <div className='content-links'>

                            <div className='header-links'>

                                <div className='link'>


                                    <a className='link-a' onClick={handleOpenSubMenuMobile}  href="/" title='CURSOS'>
                                        CURSOS
                                        <img className='seta-mobile' src={setaMobile} alt="" />
                                    </a>
                                

                                    <div className={`${'submenu '} ${subMenuMobile && 'is-open'}`}>


                                        <div className='submenu-return'>

                                            <img className='submenu-return__image' src={voltarMenuMobile} alt="" />
                                            <button onClick={handleCloseSubMenuMobile}  className='submenu-return__button'>

                                                VOLTAR
                                            </button>

                                        </div>  

                                        <div className='submenu-content'>
                                            

                                            <h2 className='submenu-content__title'>CURSOS</h2>

                                            <div className='submenu-content__wrapper'>

                                                <ul className='submenu-categories'>
                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>

                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>

                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>

                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>

                                                </ul>

                                                <ul className='submenu-categories'>
                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>

                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>

                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>

                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>
                                                </ul>
                                            </div>   
                                            
                                            <a className='submenu-content__see-all' href="/">VER TODOS</a>     
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div className='header-links'>

                                <div className='link'>

                                    <a onClick={handleOpenSubMenuMobile2} className='link-a' href="/" title='SAIBA MAIS'>
                                        SAIBA MAIS
                                        <img className='seta-mobile' src={setaMobile} alt="" />
                                    </a>

                                    <div className={`${'submenu2 '} ${subMenuMobile2 && 'Y-Open'}`}>
                                        
                                            <div className='submenu-return'>

                                                <img className='submenu-return__image' src={voltarMenuMobile} alt="" />
                                                <button onClick={handleCloseSubMenuMobile2} className='submenu-return__button'>


                                                    VOLTAR

                                                </button>

                                            </div>  

                                        <div className='submenu-content'>


                                            <h2 className='submenu-content__title'>SAIBA MAIS</h2>

                                            <div className='submenu-content__wrapper'>

                                                <ul className='submenu-categories'>

                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>

                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>

                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>

                                                    <li className='submenu-category'>
                                                        <a className='submenu-category-link' href="/">lorem ipsum <img src={setaMobile} alt="" /></a>
                                                    </li>
                                                    
                                                </ul>

                                            </div>   
                                                
                                        </div>

                                    </div>                                
                                </div>
                            </div>

                        </div>

                        <a className='menu-links-account' href="/account">
                            <img className='icon-menu-mobile' src={  iconMenuMobile } alt="" />
                            ENTRAR/MEUS PEDIDOS
                        </a>
                    </nav>

                </div>

            </header>
        </>
    )

 
}

